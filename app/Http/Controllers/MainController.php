<?php

namespace App\Http\Controllers;

use App\Ingridient;
use App\Product;
use Illuminate\Http\Request;
use GuzzleHttp;

class MainController extends Controller
{

    public function getIngridients()
    {
        $ingridients = Ingridient::orderBy('group_name')->paginate();
        return response()->json($ingridients);

    }

    public function test(Request $request)
    {

        $name = $request->file('image');
        $nm = $name->getClientOriginalName();
        $response = ['message' => 'ok', 'name' => $nm];
        return response()->json($response);
    }

    public function recognizePhoto(Request $request)
    {
        $client = new GuzzleHttp\Client();
        $file = $request->file('image');
        dump($file->store('images', 'public'));
        /*
        $response = $client->post(
            '/api/whitelabel/images', [
            'headers' => [
                'Content-Type'          => 'multipart/form-data',
            ],
            'multipart' => [
                [
                    'name'     => 'myFile',
                    'contents' => file_get_contents(),
                ],
            ],
        ],
            );
        */
        return response()->json(['message' => 'Пока метод не работает,но скоро точно заработает']);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function giveAdvice(Request $request)
    {
        // Получаем Ингридиенты пользователя(которые лежат у него в холодильнике)
        $data = json_decode($request->get('ingridients_ids'), true);
        $data = array_values($data);
        $data = $data[0];
        $response = [];
        $prod = [];
        // Формируем массив prod в котором ключом будет id продукта, а значением массив id ингридиентов
        $products = Product::with('ingridients')->get();
        foreach ($products as $item) {
            $ing = $item->ingridients->toArray();
            $result = array_map(function ($i) {
                return $i['id'];
            }, $ing);
            $prod[$item->id] = $result;
        }
        // Теперь будем узнавать что может приготовить пользователь

        foreach ($prod as $k => $v) {
            //dd(array_intersect($data, $v), $data);
            if (array_values(array_intersect($data, $v)) == array_values($data)) {
                array_push($response, $k);
            }
        }
        return Product::find($response);

    }


}
