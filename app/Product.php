<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function ingridients()
    {
        return $this->belongsToMany(Ingridient::class);
    }
}
